/* Copyright 2022 2ynn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "quantum.h"


#define LAYOUT_aerope( \
    K11, K12, K13, K14, K15, K16,      K51, K52, K53, K54, K55, K56, \
    K21, K22, K23, K24, K25, K26,      K61, K62, K63, K64, K65, K66, \
    K31, K32, K33, K34, K35, K36,      K71, K72, K73, K74, K75, K76, \
    K41, K42, K43, K44, K45, K46,      K81, K82, K83, K84, K85, K86  \
) \
{ \
    {K11, K12, K13, K14, K15, K16, K51, K52, K53, K54, K55, K56}, \
    {K21, K22, K23, K24, K25, K26, K61, K62, K63, K64, K65, K66}, \
    {K31, K32, K33, K34, K35, K36, K71, K72, K73, K74, K75, K76}, \
    {K41, K42, K43, K44, K45, K46, K81, K82, K83, K84, K85, K86}  \
}


#define LAYOUT_bnnuy( \
    K11, K12, K13, K14, K15, K16,     K51, K52, K53, K54, K55, K56, \
    K21, K22, K23, K24, K25, K26,     K61, K62, K63, K64, K65, K66, \
    K31, K32, K33, K34, K35, K36,     K71, K72, K73, K74, K75, K76, \
    K41, K42, K43,      K44, K47,     K80, K83,      K84, K85, K86  \
) \
{ \
    {K11, K12, K13, K14, K15,   K16,     K51, K52,   K53, K54, K55, K56}, \
    {K21, K22, K23, K24, K25,   K26,     K61, K62,   K63, K64, K65, K66}, \
    {K31, K32, K33, K34, K35,   K36,     K71, K72,   K73, K74, K75, K76}, \
    {K41, K42, K43, K44, KC_NO, K47,     K80, KC_NO, K83, K84, K85, K86} \
}
