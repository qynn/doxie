/* Copyright 2020 2ynn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum layer_names {
  _L0,
  _L1,
  _L2,
  _L3
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_L0] = LAYOUT_bnnuy(
        KC_ESC,  KC_Q,    KC_W,     KC_E,  KC_R,    KC_T,  KC_Y,   KC_U,  KC_I,     KC_O,    KC_P,     KC_BSPC,
        KC_TAB,  KC_A,    KC_S,     KC_D,  KC_F,    KC_G,  KC_H,   KC_J,  KC_K,     KC_L,    KC_SCLN,  KC_ENTER,
        KC_LSFT, KC_Z,    KC_X,     KC_C,  KC_V,    KC_B,  KC_N,   KC_M,  KC_COMM,  KC_DOT,  KC_SLSH,  KC_RSFT,
        KC_LCTL, KC_LWIN, MO(_L1),  KC_BSPC,        KC_G,  KC_E,          KC_SPC,   MO(_L2), KC_RALT,  KC_RCTL),

    [_L1] = LAYOUT_bnnuy(
        KC_TRNS,  KC_1,      KC_2,     KC_3,    KC_4,    KC_5,    KC_6,     KC_7,    KC_8,    KC_9,     KC_0,    KC_TRNS,
        KC_TRNS,  S(KC_EQL), KC_MINUS, KC_EQL,  KC_LBRC, KC_RBRC, KC_TRNS,  KC_TRNS, KC_UP,   KC_BSLS,  KC_QUOT, KC_TRNS,
        KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_LEFT, KC_DOWN, KC_RIGHT, KC_TRNS, KC_TRNS,
        KC_TRNS,  KC_TRNS,   KC_TRNS,           KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,          KC_TRNS,  KC_LALT, KC_TRNS),

    [_L2] = LAYOUT_bnnuy(
        KC_TRNS, S(KC_1), S(KC_2),     S(KC_3), S(KC_4),    S(KC_5),    S(KC_6), S(KC_7), S(KC_8), S(KC_9),    S(KC_0),    KC_TRNS,
        KC_TRNS, KC_TRNS, S(KC_MINUS), KC_TRNS, S(KC_LBRC), S(KC_RBRC), KC_TRNS, KC_TRNS, KC_TRNS, S(KC_BSLS), S(KC_QUOT), KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS,    KC_TRNS,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,    KC_TRNS,    KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS,              KC_TRNS,    KC_TRNS,    KC_TRNS, KC_TRNS,          KC_TRNS,    KC_TRNS,    KC_TRNS),

    [_L3] = LAYOUT_bnnuy(
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS,          KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,          KC_TRNS, KC_TRNS, KC_TRNS)
};
