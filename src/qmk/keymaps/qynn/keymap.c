#include QMK_KEYBOARD_H

/* layers */
enum layer_names {
    _MAIN,
    _LVL3,
    _LVL4,
    _FUNC,
};

enum custom_keycodes {
    NONE = SAFE_RANGE,
};

/* macros */
#define FUNL LT(_FUNC, KC_ESC)
#define FUNR LT(_FUNC, KC_ENTER)

/* Bootloader: LVL4 + x + TAB */
enum combos_events {QOMBO_EVENT};
const uint16_t PROGMEM qombo[] = {KC_CANCEL, KC_MENU, COMBO_END};
combo_t key_combos[COMBO_COUNT] = {[QOMBO_EVENT] = COMBO_ACTION(qombo)};

void process_combo_event(uint16_t combo_index, bool pressed) {
  switch(combo_index) {
  case QOMBO_EVENT:
    if (pressed) {
      reset_keyboard();
    }
    break;
  }
};

/* init */
bool init_done = false;
uint16_t init_timer = 0;

void keyboard_post_init_user(void) {
  init_timer = timer_read();
}

/* keymaps */
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Main Layer
 * ,------------------------------------------------------------------------------------.
 * | Tab  |   Q  |   W  |   E  |   R  |   T  ||   Y  |   U  |   I  |   O  |   P  |  '   |
 * |------+------+------+------+------+--------------+------+------+------+------+------|
 * | Esc  |   A  |   S  |   D  |   F  |   G  ||   H  |   J  |   K  |   L  |   ;  | Enter|
 * |------+------+------+------+------+------||------+------+------+------+------+------|
 * | Shift|   Z  |   X  |   C  |   V  |   B  ||   N  |   M  |   ,  |   .  |   /  | Shift|
 * |------+------+------+------+------+------++------+------+------+------+------+------|
 * | Ctrl | Win  | Alt  | LVL4 | LVL3 | Bksp ||Space | LVL3 | LVL4 | Alt  |  Win | Ctrl |
 * `------------------------------------------------------------------------------------'
 */

[_MAIN] = LAYOUT_aerope(
  KC_TAB,  KC_Q,    KC_W,    KC_E,      KC_R,      KC_T,    KC_Y,    KC_U,      KC_I,      KC_O,    KC_P,    KC_QUOT,
  FUNL,    KC_A,    KC_S,    KC_D,      KC_F,      KC_G,    KC_H,    KC_J,      KC_K,      KC_L,    KC_SCLN, FUNR,
  KC_LSFT, KC_Z,    KC_X,    KC_C,      KC_V,      KC_B,    KC_N,    KC_M,      KC_COMM,   KC_DOT,  KC_SLSH, KC_RSFT,
  KC_LCTL, KC_LWIN, KC_RALT, MO(_LVL4), MO(_LVL3), KC_BSPC, KC_SPC,  MO(_LVL3), MO(_LVL4), KC_RALT, KC_RWIN, KC_RCTL
),

/* Level 3 (Lower)

 * ,------------------------------------------------------------------------------------.
 * |  /   |   1  |   2  |   3  |   4  |   5  ||   6  |   7  |   8  |   9  |   0  |  `   |
 * |------+------+------+------+------+--------------+------+------+------+------+------|
 * | MAIN |   +  |   -  |   =  |   [  |   ]  || left |  dwn |  up  | right|   \  | Enter|
 * |------+------+------+------+------+------||------+------+------+------+------+------|
 * | Shift| ins  |  del |   ç  |   ^  |      ||A-dwn | A-up |   ~  |   .  |  é   | Shift|
 * |------+------+------+------+------+------++------+------+------+------+------+------|
 * | Ctrl | Win  | Alt  |      |      | Bksp ||Space |      |      | Alt  |  Win | Ctrl |
 * `------------------------------------------------------------------------------------'
 */
[_LVL3] = LAYOUT_aerope(
  KC_SLSH,   KC_1,      KC_2,     KC_3,      KC_4,    KC_5,    KC_6,          KC_7,        KC_8,      KC_9,     KC_0,       A(KC_QUOT),
  TG(_LVL3), S(KC_EQL), KC_MINUS, KC_EQL,    KC_LBRC, KC_RBRC, KC_LEFT,       KC_DOWN,     KC_UP,     KC_RIGHT, KC_BSLS,    KC_ENTER,
  KC_LSFT,   KC_INSERT, KC_DEL,   A(KC_C),   A(KC_V), NONE,    RALT(KC_DOWN), RALT(KC_UP), S(KC_GRV), KC_DOT,   A(KC_SLSH), KC_RSFT,
  KC_LCTL,   KC_LWIN,   KC_RALT,  NONE,     KC_BSPC, NONE,    NONE,          KC_SPC,      NONE,      KC_RALT,  KC_RWIN,    KC_RCTL
),

/* Level 4 (Higher)
 * ,------------------------------------------------------------------------------------.
 * |CANCEL|   !  |   @  |   #  |   $  |   %  ||   ^  |   &  |   *  |   (  |   )  |   ¨  |
 * |------+------+------+------+------+--------------+------+------+------+------+------|
 * | MAIN |      |  _   |      |      |   {  ||   }  | home | pgDn | pgUp | end  |   |  |
 * |------+------+------+------+------+------||------+------+------+------+------+------|
 * | Shift|      | MENU |   Ç  |   ˘  |      ||S-pgDn|S-pgUp|   `  |      |  É   | Shift|
 * |------+------+------+------+------+------++------+------+------+------+------+------|
 * | Ctrl | Win  | Alt  |      |      |      ||      |      |      | Alt  |  Win | Ctrl |
 * `------------------------------------------------------------------------------------'
 */
[_LVL4] = LAYOUT_aerope(
  KC_CANCEL, S(KC_1),  S(KC_2),     S(KC_3),    S(KC_4),    S(KC_5),    S(KC_6),    S(KC_7),    S(KC_8), S(KC_9),  S(KC_0),       S(A(KC_QUOT)),
  TG(_LVL4), NONE,     S(KC_MINUS), NONE,       S(KC_LBRC), S(KC_RBRC), KC_HOME,    KC_PGDN,    KC_PGUP, KC_END,   S(KC_BSLS),    NONE,
  KC_LSFT,   NONE,     KC_MENU,     S(A(KC_C)), S(A(KC_V)), NONE,       S(KC_PGDN), S(KC_PGUP), KC_GRV,  NONE,     S(A(KC_SLSH)), KC_RSFT,
  KC_LCTL,   KC_LWIN,  KC_RALT,     NONE,       NONE,       NONE,       NONE,       NONE,       NONE,    KC_RALT,  KC_RWIN,       KC_RCTL
),

/* function (long press on either Escape or Enter)
 * ,------------------------------------------------------------------------------------.
 * |  F12 |  F1  |  F2  |  F3  |  F4  |  F5  ||  F6  | F7   | F8   | F9   | F10  |  F11 |
 * |------+------+------+------+------+--------------+------+------+------+------+------|
 * |      |      |prtScr|      |      |      ||      | VOL- | VOL+ |      |      |      |
 * |------+------+------+------+------+------||------+------+------+------+------+------|
 * |      | SLEEP|      |C-A-c |C-A-v |      ||      | mute |      |      |      |      |
 * |------+------+------+------+------+------++------+------+------+------+------+------|
 * |      |      |      |LOCK4 |LOCK3 |      ||      | LOCK3| LOCK4|      |      |      |
 * `------------------------------------------------------------------------------------'
 */
[_FUNC] = LAYOUT_aerope(
  KC_F12,    KC_F1,   KC_F2,   KC_F3,         KC_F4,         KC_F5, KC_F6, KC_F7,     KC_F8,     KC_F9, KC_F10, KC_F11,
  NONE,      NONE,    KC_PSCR, NONE,          NONE,          NONE,  NONE,  KC_VOLD,   KC_VOLU,   NONE,  NONE,   NONE,
  NONE,      KC_SLEP, NONE,    C(RALT(KC_C)), C(RALT(KC_V)), NONE,  NONE,  KC_MUTE,   NONE,      NONE,  NONE,   NONE,
  NONE,      NONE,    NONE,    TG(_LVL4),     TG(_LVL3),     NONE,  NONE,  TG(_LVL3), TG(_LVL4), NONE,  NONE,   NONE
)

};


void matrix_scan_user(void) {
  /* init */
  if(!init_done & (timer_elapsed(init_timer) > 1000)){
    SEND_STRING(SS_LWIN(SS_RALT(SS_TAP(X_K))));
    // reload ~/.Xkeymap
    /* SEND_STRING(SS_LWIN(SS_TAP(X_ENTER)) SS_DELAY(500) SS_LWIN("x")); */
    init_done = true;
  }
};


bool encoder_update_user(uint8_t index, bool clockwise) {
  if (index == 0) {
    if (clockwise) tap_code_delay(KC_VOLU, 10);
    else tap_code_delay(KC_VOLD, 10);
  }
  return false;
}


/* * KEYCODE PROCESS EXAMPLE  * */
/* bool process_record_user(uint16_t keycode, keyrecord_t *record) { */
/*   switch (keycode) { */
/*   case NONE: */
/*     if (record->event.pressed) { */
/*       /\*do something?*\/ */
/*     } */
/*     return false; */
/*     break; */
/*   } */
/*   return true; */
/* } */
