# Doxie

![doxie](todo)

[Doxie][Doxie] is a 40% vertically staggered mechanical keyboard.


* Keyboard Maintainer: [2ynn][2ynn]
* Hardware Supported: Doxie PCB (Pro Micro, Elite-C)
* Hardware Availability: [PCB][PCB], [Case][Case]

Make example for this keyboard (after setting up your build environment):

    make qynn/doxie:qynn:flash

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).


[Doxie]: https://gitlab.com/qynn/doxie
[2ynn]: https://gitlab.com/qynn
[PCB]: https://gitlab.com/qynn/doxie/-/tree/main/pcb
[Case]: https://gitlab.com/qynn/doxie/-/tree/main/fab/export
