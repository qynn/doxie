/*
Copyright 2022 2ynn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "config_common.h"

/* USB Device descriptor parameters */
#define VENDOR_ID 0x1209
#define PRODUCT_ID 0xACAB // TODO https://pid.codes/howto/
#define DEVICE_VER 0x0001
#define MANUFACTURER 2ynn
#define PRODUCT doxie

/* AVR pins used for rows, top to bottom */
#define MATRIX_ROWS 4
#define MATRIX_ROW_PINS { F4, F5, D1, D0 }

/* AVR pins used for columns, left to right */
#define MATRIX_COLS 12
#define MATRIX_COL_PINS { F6, F7, B1, B3, B2, B6, D4, C6, D7, E6, B4, B5 }

#define UNUSED_PINS

/* Diode direction (Anode -->|-- Cathode)
 * COL2ROW: COL = Anode(+), ROW = Cathode(-)
 * ROW2COL: ROW = Anode(+), COL = Cathode(-)
 */
#define DIODE_DIRECTION COL2ROW

/* Prevents unintended double-presses (set to 0 if not needed) */
#define DEBOUNCE 5

/* Encoder */
#define ENCODERS_PAD_A { D3 }
#define ENCODERS_PAD_B { D2 }
/* #define ENCODER_RESOLUTION 4 */
/* #define ENCODER_DEFAULT_POS 0x3 */
