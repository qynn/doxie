# https://docs.qmk.fm/#/config_options?id=the-rulesmk-file

MCU = atmega32u4

BOOTLOADER = atmel-dfu   # Elite-C
# BOOTLOADER = caterina  # Pro Micro

# Press ESC (0,0) while plugging keyboard to jump to bootloader
# Warning: will also erase EEPROM !
BOOTMAGIC_ENABLE = yes  # Bootmagic Lite

EXTRAKEY_ENABLE = yes    # Audio and System control
ENCODER_ENABLE = yes     # Volume knob
MOUSEKEY_ENABLE = no     # Mouse keys

LAYOUTS = aerope bnnuy

