# Doxie

![cc-by-nc-sa-shield](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)

40% ergonom-ish keyboard using through-hole components only.

<img alt="doxxed-top" src="img/doxxed/doxxed_top.jpg" width="800"/>
<img alt="doxxed-back" src="img/doxxed/doxxed_back.jpg" width="800"/>
<img alt="aerope-top" src="img/aerope/aerope_top.jpg" width="800"/>


<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Doxie](#doxie)
    - [Features](#features)
    - [Layouts](#layouts)
        - [Doxxed](#doxxed)
        - [Aerope](#aerope)
        - [Bnnuy pro model](#bnnuy-pro-model)
    - [Keymaps](#keymaps)
        - [Qynn](#qynn)
        - [Bnnuy](#bnnuy)
    - [PCB](#pcb)
    - [Plates](#plates)
        - [Standoff plates](#standoff-plates)
        - [Stacked frames](#stacked-frames)
    - [Firmware](#firmware)
        - [QMK (ATmega32u4)](#qmk-atmega32u4)
        - [VIA (Bnnuy layout)](#via-bnnuy-layout)
    - [Rationale](#rationale)
    - [License](#license)
    - [Disclaimer](#disclaimer)
    - [Credits](#credits)

<!-- markdown-toc end -->

## Features

- multiple layout options
- 48 keys, vertically staggered
- 1 rotary encoder
- easy to solder (through-hole components exclusively)
- simple mechanical assembly (top + bottom plates, standoffs)
- compatible with ErgoDox keycap set
- compatible with Pro Micro (ATmega32u4) and [nice!nano](https://nicekeyboards.com/nice-nano/) (TODO)


## Layouts

### Doxxed

<img alt="layout-doxxed" src="doc/layout/doxxed.png" width="800"/>


### Aerope

<img alt="layout-aerope" src="doc/layout/aerope.png" width="800"/>


### Bnnuy pro model

<img alt="layout-bnnuy" src="doc/layout/bnnuy.png" width="800"/>


Note: in the last configuration the encoder switch can be used


## Keymaps

### Qynn

<img alt="keymap-qynn" src="doc/keymap/qynn.png" width="800"/>


### Bnnuy

<img alt="keymap-bnnuy" src="doc/keymap/bnnuy.png" width="800"/>


Feel free to submit alternative keymaps!


## PCB

<img alt="doxie-pcb" src="doc/pcb/doxie-pcb.png" width="800"/>

- [schematics](doc/pcb/doxie-sch.pdf)
- [gerbers](pcb/gbr/doxie_98mm_275mm.zip)
- [step file](fab/doxie-pcb.step)


|  Qty | Value           | Comments                                                                 |
| :--: | :--             | :--                                                                      |
|    1 | Microcontroller | [Pro Micro][pro-micro] (ATmega32u4) **or** [nice!nano_v2.0][nice-nano]     |
|    1 | Rotary Encoder  | [PEC11R-4xxxF-sxxx][pec11-s] **or** [PEC11R-4xxxF-Nxxx][pec11-N] (no switch) |
|   48 | Diodes          | General purpose 1N4148 DO-35                                             |
|   48 | Switches        | Cherry MX footprint                                                      |


[pro-micro]: https://www.digikey.ca/en/products/detail/sparkfun-electronics/DEV-12640/5140825
[nice-nano]: https://clicketysplit.ca/products/nice-nano-v2
[pec11-n]: https://www.digikey.ca/en/products/detail/bourns-inc/PEC11R-4215F-N0024/4499663
[pec11-s]: https://www.digikey.ca/en/products/detail/bourns-inc/PEC11R-4215F-S0024/4499665


## Plates

[FreeCAD source](fab/doxie.FCStd)

### Standoff plates

<img alt="standoff-plates" src="fab/export/standoff_plates/standoff_plates.svg" width="800"/>

|  Qty | Value                        | Comments                                         |
| :--: | :--                          | :--                                              |
|    1 | [Bottom plate][bottom_plate] | 1/8"                                             |
|    1 | [Switch plate][switch_plate] | 1/16"                                            |
|    1 | [MCU cover][MCU_cover]       | 1/8"                                             |
|    9 | M2 male-female standoff      | [M2x6mm+3][M2x6mm+3] (alt. [M2x8mm+3][M2x8mm+3]) |
|    9 | M2 button head screws        | [M2x6mm][M2x6mm]                                 |
|    2 | Rubber bumpers               | [1/8"x3/4"][bumpers]                         |
|    2 | M3x6mm screws                | to hold bumpers                              |

[bottom_plate]: fab/export/standoff_plates/bottom_plate/
[switch_plate]: fab/export/standoff_plates/switch_plate/
[MCU_cover]: fab/export/standoff_plates/mcu_cover/
[M2x6mm+3]: https://www.newegg.ca/p/34F-007S-00199
[M2x8mm+3]: https://www.newegg.com/p/1HD-002K-003T3
[M2x6mm]: https://www.newegg.com/p/0PS-008N-06JU7
[M2x5mm]: https://www.newegg.com/p/0PS-008N-06YE8
[bumpers]: https://www.mcmaster.com/9540K901/


### Stacked frames

<img alt="stacked-bnnuy" src="fab/export/stacked_frames/bnnuy_stacked.svg" width="800"/>

|  Qty | Value                         | Comments                                    |
| :--: | :--                           | :--                                         |
|    1 | [Floor plate][0_floor_plate]  | 1/8"                                        |
|    1 | [Bottom edge][1_bottom_edge]  | 1/4" (or 3/8")                              |
|    1 | [Switch plate][2_switch_plate]| 1/16"                                       |
|    1 | [Lower frame][3_lower_frame]  | 1/8"                                        |
|    1 | [Upper cap][4_upper_cap]      | 1/4"                                        |
|    3 | M2 male-female standoff       | [M2x6mm+3][M2x6mm] (alt. [M2x8mm+3][M2x8mm])|
|    3 | M2 button head screws         | [M2x5mm][M2x5mm]                            |
|    9 | M3 button head screws         | M3x16mm (alt. M3x20mm)                      |
|    2 | Rubber bumpers                | [1/8"x3/4"][bumpers]                        |
|    2 | M3x6mm screws                 | to hold bumpers                             |

[0_floor_plate]: fab/export/stacked_frames/0_floor_plate/
[1_bottom_edge]: fab/export/stacked_frames/1_bottom_edge/
[2_switch_plate]: fab/export/stacked_frames/2_switch_plate/
[3_lower_frame]: fab/export/stacked_frames/3_lower_frame/
[4_upper_cap]: fab/export/stacked_frames/4_upper_cap/


## Firmware

### QMK (ATmega32u4)

[README](src/qmk/readme.md)

0. Set appropriate `BOOTLOADER` in [rule.mk](src/qmk/rules.mk)
1. Check available [keymaps](src/qmk/keymaps)
2. Compile/upload firmware:
```
$ make qynn/doxie:<keymap>:flash

```

### VIA (Bnnuy layout)

<img alt="bnnuy-via" src="doc/keymap/bnnuy-via.png" width="800"/>

0. Flash [VIA-enabled firmware](src/qmk/bin/qynn_doxie_via.hex)
1. Activate Design Tab in Settings
2. Load [Bnnuy JSON layout](src/qmk/layouts/bnnuy-via.json) in Design Tab


## Rationale

I had been using the [Keebio Levinson](https://keeb.io/products/nyquist-keyboard) (aka [Levine](https://gitlab.com/qynn/levine)) as my daily driver but was interested in exploring something a bit more ergonomic.

I wouldn't want to have less than 12 keys on the bottom row because I like to have a symmetrical set of mod keys on each side, as shown in my [current layout](https://gitlab.com/qynn/levine/-/blob/rev35/layout/levine.png).

The [ErgoDash mini](https://github.com/omkbd/ErgoDash/tree/master/mini) or [ErgoTravel](https://github.com/jpconstantineau/ErgoTravel) would fit the bill, although I don't think I'd need extra keys for the brackets as I have happily remapped them to `F` and `G`

I was also inspired by the [Atreus](https://gitlab.com/technomancy/atreus) and [Reviung](https://github.com/gtips/reviung) for their "unsplit" setup, avoiding the use of two microcontrollers.

Being a i3-wm user I rely heavily on the `Win` key, which i press with my palm using a reversed R1 keycap to raise it above other keys (inspired by this [emacs user](https://www.reddit.com/r/emacs/comments/7zvw2b/my_weapon_against_emacs_pinky/)).


## License

**Doxie** mechanical keyboard by [2ynn](https://gitlab.com/qynn) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This design may be freely modified and manufactured for **PERSONAL** use only and should **NOT** be reproduced for commercial purposes.

[cc-by-nc-sa-image](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)


## Disclaimer

All files are provided *as-is*, without liability or any guarantees, including regarding functionality or fitness for a particular purpose.


## Credits

- Powered by [QMK Firmware](https://qmk.fm/)
- Proudly made with [KiCad](https://www.kicad.org/about/kicad/) and [FreeCAD](https://wiki.freecadweb.org/About_FreeCAD)
- [JSON layout](pcb/layouter.json) generated using [keyboard-layout-editor](http://www.keyboard-layout-editor.com/) by [ijprest](https://github.com/ijprest)
- Automatic switch positioning achieved using [keyboard-layouter](https://github.com/darakuneko/keyboard-layouter) Kicad plugin by [darakuneko](https://github.com/darakuneko)
- [nice!nano footprint](pcb/mod/doxie.pretty/MCU-NiceNano.kicad_mod) adapted from [bstiq](https://github.com/bstiq/nice-nano-kicad)
- [PEC11 encoder footprint](pcb/mod/doxie.pretty/Encoder-PEC11R.kicad_mod) adapted from [SnapEDA](https://www.snapeda.com/parts/PEC11R-4215F-S0024/Bourns/view-part/?t=PEC11)
- Thanks to [DIYke_boards](https://www.twitch.tv/diyke_boards) for her feedback.
